import 'package:flutter/foundation.dart';
import 'package:sqflite/sqflite.dart';
import 'Enum.dart';
import 'GlobalUser.dart';
import 'Message.dart';
import 'Data.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:typed_data';
import 'ConnectionController.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io';
import 'MessagesList.dart';
import 'Convert.dart';
import 'MessageFile.dart';
import 'package:nearby_connections/nearby_connections.dart';
import 'package:image_picker/image_picker.dart';


class Conversation {
  List<Widget> _messages = [];
  List<GlobalUser> _users = [];
  int _id;
  bool isOffline = false;
  String title;

  get id{
    return _id;
  }

  List<Widget> get messages {
    return _messages;
  }

  List<GlobalUser> get users {
    return _users;
  }

  Conversation({bool isNew, int id, @required String this.title, bool this.isOffline, GlobalUser user, List<GlobalUser> users, bool isPrivate }){
    if (id != null && (isNew == null || !isNew) ) {
      _id = id;

      if (id != 0) { //id 0 is the default local conversation

        loadUsers(id: _id).then((val) {
          _users = val;

          if (_users.length == 2) {
            _users[1].privateConversation = this;
          }

          loadMessages(id: id).then((val) {
            _messages = val;
          });
        });
      }
    }
    else if (user != null && id == null){
      _id = Data.random.nextInt(Constants.max_id); //TODO : insert conversation in database
      _users.add(Data.globalUser);
      _users.add(user);
      
      List<int> data = [];
      data.add(PacketType.easy.index);
      data.add(DataType.conversation.index);


      data.add(DataType.conversationId.index);
      data.addAll(Convert.encodeInt32(_id));
      data.add(DataType.conversationType.index);
      data.add(ConversationType.private.index);
      data.addAll(Convert.encodeGlobalUser(Data.globalUser));
      data.add(DataType.globalUserList.index);
      data.addAll(Convert.encodeGlobalUserList(_users));

      ConnectionController.sendPacketToGlobal(user, data); //TODO : change to send packet to all when group conversations will be implemented

      insert();
    }
    else if (isNew != null && isNew && user != null && isPrivate != null && isPrivate) {
      _id = id;

      _users.add(Data.globalUser);
      _users.add(user);

      insert();
    }
    else {
      print('Unhandled conversation constructor');
    }
    //TODO : implement other types of conversations
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
    };
  }

  Future<void> insert() async {
    await Data.database.insert('conversation', this.toMap(), conflictAlgorithm: ConflictAlgorithm.replace);
    for (GlobalUser user in users) {
      await Data.database.insert('conversation_user', {'idconversation':id, 'iduser':user.id});
    }
  }

  Future<List<Widget>> loadMessages({@required int id}) async {
    final List<Map<String, dynamic>> maps = await Data.database.query('message', where: 'idconversation = ?', whereArgs : [id]);

    return List.generate(maps.length, (i) {
      GlobalUser user = GlobalUser.findUserById(_users, maps[i]['iduser']);

      if (user == null) {
        print('ERROR : Conversation contains null user');
      }

      Data.listKey.currentState.insertItem(i);
      Data.listKey.currentState.insertItem(i);

      return Message(
        id : maps[i]['id'],
        name: user.name,
        message: maps[i]['message'],
        date: maps[i]['date'],
        user : user,
        conversation: this,
      );

    });
  }

  static Future<List<GlobalUser>> loadUsers({@required int id}) async {
    final List<Map<String, dynamic>> maps = await Data.database.query('conversation_user', where: 'idconversation = ?', whereArgs: [id]);

    return List.generate(maps.length, (i) {
      GlobalUser user = GlobalUser.findUserById(Data.users, maps[i]['iduser']);
      if ( user == null) {
        throw new Exception('ERROR : user id '+maps[i]['iduser']+' not found in user list. Was a conversation loaded before GlobalUsers ?');
      }
      return user;
    });
  }

  add(Widget message) {
    _messages.add(message);

    Data.listKey.currentState.insertItem(_messages.length - 1);
    Data.listKey.currentState.insertItem(_messages.length);

    if (message is Message && id != 0) {
      message.insert();
    }

    if (this == Data.currentConversation) {
      final scrollController = (Data.listKey.currentContext.ancestorStateOfType(new TypeMatcher()) as MessagesListState);
      Timer(new Duration(milliseconds: 50), () { //without this delay, the last message isn't loaded in the listview properly and it doesn't scrolls properly to the last message
        scrollController.scrollToBottom();
      });
    }
  }

  int nextId() {
    return _messages.length+1;
  }

  send(String messageString, {File file} ) {
    Message message;
    if (file != null) {
      message = Message(name : Data.user.name, message : messageString, conversation: this, id: this.nextId(), user: Data.globalUser,file : file ,);
    }
    else {
      message = Message(name : Data.user.name, message : messageString, conversation: this, id: this.nextId(), user: Data.globalUser,);
    }

    add(message);

    if (_id == 0) {
      ConnectionController.sendMessage(messageString);
      //TODO : implement messageId sending in order o be able to properly attach images to corresponding messages

      if (file != null) {
        Data.lanUsers.forEach((user) async {
          //int payloadId = await Nearby().sendFilePayload(user.lan_id, file.path);
          ConnectionController.sendMessageFileTo(user.lan_id, MessageFile(file, message: message));


          print('FINI !');
        });
      }
      print('This is local conversation');
    }
    else {
      print('Sending complex message in conversation '+_id.toString());
      users.forEach((user) {
        print(user.id);
      });

      List<int> data = [];
      data.add(PacketType.easy.index);
      data.add(DataType.complex_message.index);
      data.add(DataType.conversationId.index);
      data.addAll(Convert.encodeInt32(_id));
      data.add(DataType.id.index);
      data.addAll(Convert.encodeInt32(Data.globalUser.id));
      data.add(DataType.message.index);
      data.addAll(Convert.encodeString(messageString));

      //TODO : implement storing message in to_be_sent_list if unable to send it
      ConnectionController.sendPacketToAllGlobal(users, data, doNotSendList: [Data.globalUser]);
      if (file != null) {
        //ConnectionController.sendFileToAllGlobal(users, image.path, doNotSendList: [Data.globalUser]);


        //TODO : put this elsewhere
      }

    }
  }


  static Conversation findConversationById(List<Conversation> conversations, int id) {
    Conversation theConversation;
    for (Conversation conversation in conversations) {
      if (conversation.id == id) {
        theConversation = conversation;
        break;
      }
    }
    return theConversation;
  }
}