

enum DataType {
  message,
  name,
  userlist,
  conversationId,
  complex_message,
  endpointid,
  endIdRequest,
  id,
  conversation,
  globalUserList,
  globalUser,
  conversationTitle,
  conversationType,
  length,
  string,
}

enum PacketType {
  easy,
  fileInfo,
  broadcast,
  targetlist,
  target,
  newconnection,
  deadconnection,
}

enum ConversationType {
  private,
  group
}

class Constants {
  static const lan_id_size = 8;
  static const debug_log = false;
  static const DBCreateTableThisUser = "CREATE TABLE this_user (id INTEGER, name TEXT);";
  static const DBCreateTableUser = "CREATE TABLE user (id INTEGER, name TEXT, pub_key TEXT, private_conversationid INTEGER); ";
  static const DBCreateTableConversation = "CREATE TABLE conversation(id INTEGER, temp_id INTEGER, title TEXT); ";
  static const DBCreateTableConversationUser = "CREATE TABLE conversation_user(idconversation INTEGER, iduser INTEGER); ";
  static const DBCreateTableMessage = "CREATE TABLE message(id INTEGER, idconversation INTEGER, iduser INTEGER, date TEXT, message TEXT);";
  static const max_id = 4294967295;
}