
import 'package:nearby_connections/nearby_connections.dart';
import 'Message.dart';
import 'dart:typed_data';
import 'dart:core';
import 'Enum.dart';
import 'Data.dart';
import 'User.dart';
import 'dart:async';
import 'dart:io';
import 'Convert.dart';
import 'GlobalUser.dart';
import 'Conversation.dart';
import 'MessageFile.dart';

class ConnectionController {

  static List<String> tryingToConnectId = [];
  static List<String> connectedId = [];
  static String username = 'Bibi';

  static start() async {
    try {
      startDiscovery();

    } catch (exception) {
      print('CONNECTION EXCEPTION : '+exception.toString());
    }
  }

  static startDiscovery() async{
    if (Data.isDiscovering) {
      Nearby().stopDiscovery();
      Data.isDiscovering = false;
    }

    if(ConnectionController.connectedId.isEmpty && !Data.isDiscovering) {
      Data.isDiscovering = true;
      await Nearby().startDiscovery(
        'test',
        Strategy.P2P_CLUSTER,
        onEndpointFound: (String id,String userName, String serviceId) {
          processEndPointFound(id, userName, serviceId);
        },
        onEndpointLost: (String id) {

        },
      );

      if(Data.isAdvertising && ConnectionController.connectedId.isNotEmpty) {
        Nearby().stopAdvertising();
        Data.isAdvertising = false;
      }

    }
    Data.timer = Timer(Duration(seconds: (Data.random.nextInt(10) +2 )), () {
      startAdvertising();
    });

  }

  static startAdvertising() async {

    if (ConnectionController.connectedId.isEmpty || !Data.isAdvertising) {
      if (!Data.isAdvertising)
      {
          await Nearby().startAdvertising(
            'test',
            Strategy.P2P_CLUSTER,
            onConnectionInitiated: (String id,ConnectionInfo info) {
              Nearby().stopAdvertising();
              Data.isAdvertising = false;
              Nearby().stopDiscovery();
              Data.isDiscovering = false;
              processConnectionInitiated(id, info);
            },
            onConnectionResult: (String id,Status status) {
              processConnectionResult(id, status);
            },
            onDisconnected: (String id) {
              processDisconnect(id);
            },
          );
          Data.isAdvertising = true;
      }

    }
    if (Data.isDiscovering) {
      Nearby().stopDiscovery();
      Data.isDiscovering = false;
    }

    Data.isDiscovering = false;

    Data.timer = Timer(Duration(seconds: (Data.random.nextInt(5) +5 )), () {
      startDiscovery();
    });

  }

  static processEndPointFound (String id, String userName, String serviceId) async{
    try{
      if (!tryingToConnectId.contains(id.toString()) && !connectedId.contains(id.toString())) {
        Data.timer.cancel();
        Nearby().stopAdvertising();
        Nearby().stopDiscovery();

        if (Constants.debug_log)
          print('ENDPOINT ID : '+id);

        Nearby().requestConnection(
          username,
          id,
          onConnectionInitiated: (id, info) {
            processConnectionInitiated(id, info);
          },
          onConnectionResult: (id, status) {
            processConnectionResult(id, status);
          },
          onDisconnected: (id) {
            processDisconnect(id);
          },
        );
      }
       else {
         if (Constants.debug_log)
          print('Endpoint '+id+' found but already connected');
      }

    }catch(exception){
      print('CONNECTION EXCEPTION : '+exception.toString());
    }
  }

  static processConnectionInitiated(String id, ConnectionInfo info) async{
    try {
      if (!tryingToConnectId.contains(id.toString()) && !connectedId.contains(id.toString())) {
        tryingToConnectId.add(id.toString());
        if (Constants.debug_log)
          print('Connection established with '+id);
        Nearby().acceptConnection(
            id,
            onPayloadTransferUpdate: (endid, payloadTransferUpdate) {
              processPayloadTransfertUpdate(endid, payloadTransferUpdate);
            },
            onPayLoadRecieved: (endid, Payload bytes) {
              processPayLoad(endid, bytes);
            }
        );
      }
      else {
        Nearby().rejectConnection(id);
      }
    }
    catch(exception)
    {
      tryingToConnectId.remove(id.toString());
      print('WTF exception : '+exception.toString());
    }
  }

  static processDisconnect(id) {
    try {

      if (Constants.debug_log) {
        print('Apparently lost connection to '+id);
      }
      if (connectedId.contains(id.toString()))
        connectedId.remove(id.toString());
      if (tryingToConnectId.contains(id.toString()))
        tryingToConnectId.remove(id.toString());

      if (Constants.debug_log) {
        print('Disonnecting from endpoint '+id);
      }
      Nearby().disconnectFromEndpoint(id);

      if (Constants.debug_log) {
        print('Searching for user '+id);
      }
      User user = User.findUserByLanId(Data.lanUsers, id);

      if (Constants.debug_log) {
        print('Trying to find a new route to reach '+id);
      }


      if (user != null) {
        print('Found user we got disconnected from');
        Data.lanUsers.remove(user);
        user.directlyConnected = false;

        if (!findRouteTo(user.lan_id, avoidUser: user)) {

          if (Constants.debug_log) {
            print('Removing '+id+' from our connected users list');
          }

          user.connected = false;
          if (Data.user.connectedTo.contains(user))
            Data.user.connectedTo.remove(user);
          if (user.connectedTo.contains(user))
            user.connectedTo.remove(user);

          Data.user.connectedTo.forEach((wowUser) {
            if (wowUser.route == id) {
              manageNewRouteFinding(wowUser, id);
            }
          });

          if (Constants.debug_log) {
            print('Sending that we got disconnected from '+id);
          }
          sendDeadConnectionNotice(id);


          if (Constants.debug_log) {
            print('Direct connection to '+id+' lost');
          }
        }
        else {
          if (Constants.debug_log) {
            print('Direct connection to ' + id + ' lost, but still able to reach him through ' + user.route);
          }
        }
      }

    }
    catch(exception) {
      print('Erreur : '+exception);
    }
  }



  static processConnectionResult(id, status) {
    try {
      if (Constants.debug_log)
        print('Processing connection result...');

      if (status == Status.CONNECTED) {

        if (Constants.debug_log)
          print('Removing user from the trying to connect list');

        tryingToConnectId.remove(id.toString());

        if (Constants.debug_log)
          print('Adding user to the connected userid list');

        connectedId.add(id.toString());
        sendIdRequest(id);

        User user = new User();
        user.lan_id = id;
        user.directlyConnected = true;
        user.name = null;

        if (Constants.debug_log)
          print('Adding user to the connected users list');

        if(Data.user.addConnectedUser(user)) {
          onNewLanConnectedUser(user.lan_id);
        }

      }
      else {
        tryingToConnectId.remove(id.toString());
      }
      start();
    }
    catch(exception)
    {
      print('ERROR '+exception.toString());
    }

  }


  static processPayLoad(endid, Payload bytes) async{
    print(bytes.type.toString() + 'WOWOWO');
    if (bytes.type == PayloadType.BYTES) {
      if (Constants.debug_log)
        print('Received payload of lenght '+bytes.bytes.length.toString() +'!! '+Convert.decodeString(bytes.bytes));

      if (Data.user.lan_id == null) {
        if (bytes.bytes[0] == PacketType.easy.index) {
          if (bytes.bytes[1] == DataType.endIdRequest.index) {
            processIdRequest(endid);
          }
          else if (bytes.bytes[1] == DataType.endpointid.index) {
            if (Data.user.lan_id == null) {
              Data.user.lan_id = Convert.decodeString(bytes.bytes.sublist(2, bytes.bytes.length));
              print('Looks like we are '+Data.user.lan_id);
              if(Constants.debug_log) {
                Data.lanConversation.add(SystemMessage('My id id '+Data.user.lan_id));
              }
            }
            else if (Data.user.lan_id != Convert.decodeString(bytes.bytes.sublist(2, bytes.bytes.length))) {
              print('ERROR !!! RECEIVED ID '+Convert.decodeString(bytes.bytes.sublist(2, bytes.bytes.length))+' WHEREAS WE THOUGH WE WERE '+Data.user.lan_id);
            }
          }
          else {
            if (Constants.debug_log)
              print('Waiting to receive our id to process payload from '+endid);
            sendIdRequest(endid);
            Timer(Duration(seconds: 1), () => processPayLoad(endid, bytes));
          }
        }
        else {
          if (Constants.debug_log)
            print('Waiting to receive our id to process payload from '+endid);
          sendIdRequest(endid);
          Timer(Duration(seconds: 1), () => processPayLoad(endid, bytes));
        }
      }
      else {
        if (bytes.bytes[0] == PacketType.easy.index) {
          if (Constants.debug_log)
            print('Payload is easy type');
          if (bytes.bytes[1] == DataType.message.index) {
            if (Constants.debug_log)
              print('Payload is message');
            String name;
            User user = User.findUserByLanId(Data.user.connectedTo, endid);
            if (user != null) {
              name = user.name;
            }
            else {
              name = 'Bibi';
            }
            print(Convert.decodeString(bytes.bytes.sublist(2, bytes.bytes.length)));
            Data.lanConversation.add(Message(name : name/*, user : user.globalUser*/ ,message : Convert.decodeString(bytes.bytes.sublist(2, bytes.bytes.length)), conversation: Data.lanConversation,));
          }
          else if (bytes.bytes[1] == DataType.name.index) {
            if (Constants.debug_log)
              print('Payload is name');
            User user = User.findUserByLanId(Data.user.connectedTo, endid);
            if (user != null) {
              String newname = Convert.decodeString(bytes.bytes.sublist(2, bytes.bytes.length));
              if (Constants.debug_log)
                print('new name of '+endid+' is '+newname);
              user.name = newname;
            }
            else {
              //print('Error : received name from unknown user '+endid);
              Timer(Duration(seconds: 1), (){
                processPayLoad(endid, bytes);
              }); //TODO : Make this less ugly
            }
          }
          else if (bytes.bytes[1] == DataType.endIdRequest.index) {
            print('Payload is id request');
            processIdRequest(endid);
          }
          else if (bytes.bytes[1] == DataType.endpointid.index) {
            print('Payload is id info');
            if (Data.user.lan_id == null) {
              Data.user.lan_id = Convert.decodeString(bytes.bytes.sublist(2, bytes.bytes.length));
              print('Looks like we are '+Data.user.lan_id);
            }
            else if (Data.user.lan_id != Convert.decodeString(bytes.bytes.sublist(2, bytes.bytes.length))) {
              print('ERROR !!! RECEIVED ID '+Convert.decodeString(bytes.bytes.sublist(2, bytes.bytes.length))+' WHEREAS WE THOUGH WE WERE '+Data.user.lan_id);
            }
          }
          else if (bytes.bytes[1] == DataType.userlist.index) {
            print('Payload is UserList');
            List<String> useridList = [];
            for (int i = 0; i < (bytes.bytes.length - 2)/Constants.lan_id_size; i++) {
              //print((2 + (i * Constants.lan_id_size)).toString()+' ayaya '+(2 + ((i+1)* Constants.lan_id_size)).toString()+bytes.bytes.length.toString());

              useridList.add(Convert.decodeString(bytes.bytes.sublist(2 + (i * Constants.lan_id_size), 2 + ((i+1)* Constants.lan_id_size))));
            }
            useridList.forEach((userid) {
              if (userid != endid && userid != Data.user.lan_id)
                processNewUserConnected(userid, endid);
            });
          }
          else if (bytes.bytes[1] == DataType.id.index) {
            print('ID Packet received');
            int id = Convert.decodeInt32(bytes.bytes.sublist(2, bytes.bytes.length));
            print('Received ID is '+id.toString());

            User lanUser = User.findUserByLanId(Data.user.connectedTo, endid);
            GlobalUser globalUser = GlobalUser.findUserById(Data.users, id);

            //TODO : implement this in a function

            if (lanUser == null) {
              print('ERROR : Received packet from unknown user');
              /*lanUser = new User(name : 'Unknown name');
            globalUser = new GlobalUser(id : id, pub_key : '', name: lanUser.name);
            lanUser.lan_id = endid;
            Data.user.connectedTo.add(lanUser);
            lanUser.globalUser = globalUser;
            globalUser.lanUser = lanUser; */
            }
            else if (globalUser == null) {
              globalUser = new GlobalUser(id : id, pub_key : '', name: lanUser.name); //TODO : implement public key sending and change this line
              Data.users.add(globalUser);
              lanUser.globalUser = globalUser;
              globalUser.lanUser = lanUser;
            }
            else {
              globalUser.lanUser = lanUser;
              lanUser.globalUser = globalUser; //TODO : implement checking legitimacy of lan_user to be global_user
            }
          }
          else if (bytes.bytes[1] == DataType.conversation.index) {
            print('Payload is a new conversation');
            Conversation conversation = decodeConversation(bytes.bytes.sublist(2));
            if (conversation != null) {
              Data.conversations.add(conversation);
            }
          }
          else if (bytes.bytes[1] == DataType.complex_message.index) {
            print('Payload is a complex message');
            decodeComplexMessage(bytes.bytes.sublist(2));
          }

        }
        else if (bytes.bytes[0] == PacketType.target.index) {
          print('Payload is for a target');
          if (bytes.bytes[1] == DataType.endpointid.index) {
            if (Data.user.lan_id != null) {
              print(Convert.decodeString(bytes.bytes.sublist(2, 2+Constants.lan_id_size)));
              String target_id = Convert.decodeString(bytes.bytes.sublist(2,2+Constants.lan_id_size));
              String source_id = Convert.decodeString(bytes.bytes.sublist(2+Constants.lan_id_size,2+(2*Constants.lan_id_size)));

              User relayUser = User.findUserByLanId(Data.user.connectedTo, endid);

              if (relayUser != null) {
                User sourceUser = User.findUserByLanId(relayUser.connectedTo, source_id);
                if (sourceUser != null || relayUser.lan_id == source_id) {
                  if (target_id == Data.user.lan_id) {
                    print('Processing Payload from '+source_id+' relayed by '+endid+' for me, aka '+target_id);
                    Payload payload = Payload(bytes : bytes.bytes.sublist(2+2*Constants.lan_id_size, bytes.bytes.length));
                    payload.type = PayloadType.BYTES;
                    processPayLoad(source_id, payload);
                  }
                  else {
                    if(sendPacketTo(target_id, bytes)) {
                      print('Payload for '+target_id+' successfully transmitted');
                      //TODO : Send to sender that target_id is accessible from here
                    }
                    else {
                      //TODO : Send to sender that target_id isn't accessible from here
                    }
                  }
                }
                else {
                  Timer(Duration(seconds: 1), () {
                    processPayLoad(endid, bytes);
                  });
                  print('ERROR : delayed payload processing because we cannot know the route this packet took');
                }
              }
              else {
                Timer(Duration(seconds: 1), () {
                  processPayLoad(endid, bytes);
                });
                print('ERROR : delayed payload processing because we dont know the source of this packet');
              }


            }
            else {
              print('Received packet for '+Convert.decodeString(bytes.bytes.sublist(2, 2+Constants.lan_id_size))+' we could not process because our id is unknown');
            }
          }
          else {
            print('Received malformed packet which targets someone but DataType is not endid');
          }
        }
        else if (bytes.bytes[0] == PacketType.newconnection.index) {
          print('Payload is a new connection');
          if (bytes.bytes[1] == DataType.endpointid.index) {

            String id = Convert.decodeString(bytes.bytes.sublist(2,2+Constants.lan_id_size));

            processNewUserConnected(id, endid);
          }
          else {
            print('Malformed packet received');
          }
        }
        else if (bytes.bytes[0] == PacketType.deadconnection.index) {
          print('Payload is a dead connection');
          if (bytes.bytes[1] == DataType.endpointid.index) {
            String id = Convert.decodeString(bytes.bytes.sublist(2,2+Constants.lan_id_size));

            manageUserDisconnection(endid, id);
          }
          else {
            print('Malformed packet received');
          }
        }
        else if (bytes.bytes[0] == PacketType.fileInfo.index) {
          print ('Payload is file info');
          processFileInfo(bytes.bytes.sublist(1));
        }
        else{
          print('Payload is unhandled');

        }
      }
    }
    else if (bytes.type == PayloadType.FILE){
      MessageFile file = MessageFile(File(bytes.filePath),isTransferring: true);
      file.payloadId = bytes.id;

      Data.files.add(file);
      //print(bytes.filePath);
    }
    else {
      print('ERROR : Unhandled payload type received');
    }

  }

  static processPayloadTransfertUpdate(String lanid, PayloadTransferUpdate data) async {
    if (data.status == PayloadStatus.SUCCESS) {
      Data.files.forEach((file) {
        if (file.isTransferring && file.payloadId == Convert.abs(data.id)) {
          file.isTransferring = false;
          return;
        }
      });
    }
    print(data.status.toString());
  }

  static List<String> getAllRouteAncestors(User user) {
    List<String> list = [];
    if (user != null) {
      User tempUser = user;
      while (!(tempUser == null) && !tempUser.directlyConnected) {
        list.add(tempUser.lan_id);
        tempUser = User.findUserByLanId(Data.user.connectedTo, tempUser.route);
      }user.name = null;
    }
    else {
      print('ERROR : Trying to find ancestors of null user');
    }
    if (Constants.debug_log) {
      print('All route ancestors : ');
      list.forEach((qsdf) {
        print(qsdf);
      });
    }
    return list;
  }

  static bool findRouteTo(String lanid, {User avoidUser}) {
    bool success = false;
    User target = User.findUserByLanId(Data.user.connectedTo, lanid);

    if (target == null) {
      print('WUT ERROR : Trying to find a route to a non-existent user');
    }
    else {
      print('Searching for new route...');
      Data.user.connectedTo.forEach((user) {
        if (user.connectedTo.contains(target) && user.connected && !(getAllRouteAncestors(user).contains(avoidUser.lan_id.toString())) ) {
          target.route = user.lan_id;
          Data.user.connectedTo.add(target);
          target.connected = true;
          success = true;
          print(target.route);
        }
      });
    }
    print('Done searching for a route');
    return success;
  }

  static manageUserDisconnection(String endid, String id) {
    if (Constants.debug_log) {
      print(endid+' apparently disconnected from '+id);
    }

    if (id == Data.user.lan_id) {
      print('WUT ERROR : Received disconnection message that targets ourselves');
    }
    else {
      User user = User.findUserByLanId(Data.user.connectedTo, id);

      if (user==null || !user.connected) {
        print('WUT ERROR : Received disconnection message for a user we were not connected to');
      }
      else {

        User theOneWhoSentThePacket = User.findUserByLanId(Data.user.connectedTo, endid);

        if (user.connectedTo.contains(theOneWhoSentThePacket))
          user.connectedTo.remove(theOneWhoSentThePacket);
        if (theOneWhoSentThePacket.connectedTo.contains(user))
          theOneWhoSentThePacket.connectedTo.remove(user);

        manageNewRouteFinding(user, endid);
      }
    }
  }

  static manageNewRouteFinding(User user, String endid) {
    if (endid == Data.user.lan_id) {
      throw new Exception('Trying to manage route to '+user.lan_id+' without routing through us, which does not make sense');
    }

    String id = user.lan_id;
    print('Trying to find new route to reach '+user.lan_id);

    User tempUser = user;
    while(tempUser!=null && !tempUser.directlyConnected && tempUser.lan_id != endid) { //TODO : make this less awful
      tempUser = User.findUserByLanId(Data.user.connectedTo, tempUser.route);
    }

    if (tempUser == null || tempUser.lan_id == endid) {
      print('It seems the user current route is no longer working');
      if(findRouteTo(id)) {
        if(Constants.debug_log) {
          print('A new route has been found to reach'+id+' using '+user.lan_id);
        }
      }
      else {
        if(Constants.debug_log) {
          print('No new route to reach '+id+' has been found, disconnecting from him');
        }
        user.connected = false;
        if (Data.user.connectedTo.contains(user))
          Data.user.connectedTo.remove(user);
        if (Data.lanUsers.contains(user))
          Data.lanUsers.remove(user);
        sendDeadConnectionNotice(id);

        bool changedSomething = true;
        while(changedSomething) {
          changedSomething = false;
          Data.user.connectedTo.forEach((connectedUser) {
            manageNewRouteFinding(connectedUser, user.lan_id);
          });
        }
      }
    }
    else if (tempUser.directlyConnected) {
      print('We are directly connected, soooo');
      tempUser.route = null;
      findRouteTo(id);
      print("But we don't care because we were connected to it through another way");
    }
    else {
      print('Wellll, we should never have gone here, soooooooo that is reaaaally strange');
    }
  }

  static processNewUserConnected(String id, String endid) {
    if (Constants.debug_log) {
      print('Processing new user connected');
      print(endid+' apparently connected to '+id);
    }

    if (id != Data.user.lan_id) {
      print('New user is not us');

      bool isNewUser = false;

      User user = User.findUserByLanId(Data.user.connectedTo, id);
      if (user == null || user.connected ==false ) {
        print('It seems like a real new user');
        isNewUser = true;
        if (user == null) {
          user = new User();
          user.lan_id = id;
          user.name = null;
        }

        User sourceUser = User.findUserByLanId(Data.user.connectedTo, endid);
        if (sourceUser != null && sourceUser != user) {
          if (!sourceUser.connectedTo.contains(user))
            sourceUser.connectedTo.add(user);
          if (!user.connectedTo.contains(sourceUser))
            user.connectedTo.add(sourceUser);
          user.route = sourceUser.lan_id;
        }
        else {
          print('ERROR : Received new user connection from unknown user');
        }
        user.connected = true;
      }

      if (isNewUser) {
        Data.user.addConnectedUser(user);
        onNewLanConnectedUser(id);
      }
      else {
        print('Looks like we were already connected to this user... (i''m talking about '+id);
      }
    }
  }

  static sendNameTo(String target_lan_id) {
    if (Data.user.lan_id == null) {
      if(Constants.debug_log)
        print('Delayed name sending until we get our lan_id');
      Timer(Duration(seconds: 1), () {
        sendNameTo(target_lan_id);
      });
    }
    else {
      print('Sending our name to '+target_lan_id);
      sendPacketTo(target_lan_id, encodePacketForTarget(target_lan_id, encodeName(Data.user.name)));
    }
  }

  static sendId(String target_lan_id) {
    if (Data.user.lan_id == null) {
      if(Constants.debug_log)
        print('Delayed name sending until we get our lan_id');
      Timer(Duration(seconds: 1), () {
        sendId(target_lan_id);
      });
    }
    else {
      print('Sending our id to '+target_lan_id);
      List<int> data = [PacketType.easy.index, DataType.id.index];
      data.addAll(Convert.encodeInt32(Data.globalUser.id));
      sendPacketTo(target_lan_id, encodePacketForTarget(target_lan_id, data));
    }
  }

  static sendNewUserConnectedNotice(User id) {
    List<int> data = [PacketType.newconnection.index, DataType.endpointid.index];
    data.addAll(Convert.encodeString(id.lan_id) );
    print('SENDING NEW USER CONNECTED NOTICE');

    sendPayloadToAll(Uint8List.fromList(data), doNotSendList: getAllRouteAncestors(id));
  }

  static sendDeadConnectionNotice(String id) {
    if (Constants.debug_log)
      print('Sending dead connection notice for user '+id);
    List<int> data = [];
    data.add(PacketType.deadconnection.index);
    data.add(DataType.endpointid.index);
    data.addAll(Convert.encodeString(id) );

    sendPayloadToAll(Uint8List.fromList(data), doNotSendList: [id.toString()]);
    print('Dead connection notice for user '+id+' sent !');
  }

  static sendConnectedUsersList(String idTarget) {
    if (Constants.debug_log)
      print('Sending connected users list');
    List<int> data = [PacketType.easy.index, DataType.userlist.index];
    Data.user.connectedTo.forEach((user) {
      if (user.connected) {
        data.addAll(Convert.encodeString(user.lan_id));
      }
    });

    if (Data.user.lan_id == null) {
      Timer(Duration(seconds: 1), () {
        print('Delayed dending connected users list until i know my id');
        sendConnectedUsersList(idTarget);
      });
    }
    else {
      sendPacketTo(idTarget, encodePacketForTarget(idTarget, data));
    }

  }

  static Payload encodePacketForTarget(String lanid, List<int> bytes) {
    List<int> data = [PacketType.target.index, DataType.endpointid.index];
    data.addAll(Convert.encodeString(lanid) );
    data.addAll(Convert.encodeString(Data.user.lan_id) );
    data.addAll(bytes);

    Uint8List list = Uint8List.fromList(data);

    return Payload(bytes : list);
  }


  static bool sendPacketTo(String lanid, Payload bytes) {
    User target = null;

    if (Constants.debug_log) {
      print('Trying to send payload to '+lanid);
    }

    Data.lanUsers.forEach((user) {
      if (user.lan_id == lanid) {
        print('Found user '+lanid+' in direct connected user list');
        target = user;
      }
    });

    if (target != null) {
      sendPayloadToId(target.lan_id, bytes);
      if (Constants.debug_log) {
        print('Payload for '+lanid+' sent to '+target.lan_id+' directly');
      }
      return true;
    }
    else {
      Data.lanUsers.forEach((user) {
        user.connectedTo.forEach((connectedUser) {
          if (connectedUser.lan_id == lanid) {
            target = user;
          }
        });
      });

      if(target != null) {
        sendPayloadToId(target.lan_id, bytes);
        if (Constants.debug_log) {
          print('Payload for '+lanid+' sent to '+target.lan_id+' indirectly');
        }
        return true;
      }
      else {
        if (Constants.debug_log) {
          print('Unable to find route to '+lanid);
        }
        return false;
      }
    }
  }

  static sendPayloadToId(String lanid, Payload payload) async{
    Nearby().sendBytesPayload(lanid, payload.bytes);
  }

  static sendMessage(String message) {
    List<int> truc = [PacketType.easy.index, DataType.message.index];
    truc.addAll(Convert.encodeString(message));
    sendPayloadToAll(Uint8List.fromList(truc));
  }

  static encodeName(String name) {
    List<int> data = [PacketType.easy.index, DataType.name.index];
    data.addAll(Convert.encodeString(name) );
    return Uint8List.fromList(data);
  }

  static encodeId(int id) {


  }


  static sendName(String name) {

    Uint8List data = encodeName(name);
    sendPayloadToAll(data);
  }

  static sendPayloadToAllDirect(Uint8List payload) {
    connectedId.forEach((id) {
      Nearby().sendBytesPayload(id, payload);
    });
  }

  static sendPayloadToAll(Uint8List payload,  {List<String> doNotSendList}) {
    if (Data.user.lan_id == null) {
      Timer(Duration(seconds: 1), () {
        if(Constants.debug_log)
          print('Delayed payload sending to everyone until we get our lan_id');
        sendPayloadToAll(payload);
      });
    }
    else {
      print('Sending payload to all...');
      Data.user.connectedTo.forEach((user) {
        if (doNotSendList == null || !doNotSendList.contains(user.lan_id.toString())) {
          print('Sending payload to '+user.lan_id);
          sendPacketTo(user.lan_id, encodePacketForTarget(user.lan_id, payload));
          print('Payload sent to '+user.lan_id);
        }
      });
    }
  }



  static sendIdRequest(String targetId) async {
    List<int> data = [PacketType.easy.index, DataType.endIdRequest.index];
    sendPayloadToId(targetId, Payload(bytes : Uint8List.fromList(data)));
  }

  static processIdRequest(String targetId) {
    List<int> data = [PacketType.easy.index, DataType.endpointid.index];
    data.addAll(Convert.encodeString(targetId) );
    sendPayloadToId(targetId, Payload(bytes : Uint8List.fromList(data)));
  }

  static onNewLanConnectedUser(String lan_id) {
    print('OnNewLanUserConnected '+lan_id);
    if (Constants.debug_log)
      print('This seems to be a new user, sending name');
    sendNameTo(lan_id);

    if (Constants.debug_log)
      print('Name sent, sending connected users list');
    sendConnectedUsersList(lan_id);

    print('Sending our id to '+lan_id);
    Timer(Duration(seconds: 1), () {
      sendId(lan_id); //TODO : make things cleaner than relying on timing
    });
  }

  static sendPacketToGlobal(GlobalUser user, List<int> data) {
    if (user.lanUser != null && user.lanUser.lan_id != null && user.lanUser.connected) {
      sendPacketTo(user.lanUser.lan_id, encodePacketForTarget(user.lanUser.lan_id, data));
    }
    else {
      //TODO : implement online
    }
  }

  static sendPacketToAllGlobal(List<GlobalUser> users, List<int> data, {List<GlobalUser> doNotSendList}) {
    users.forEach((user) {
      if (doNotSendList == null || !doNotSendList.contains(user)) {
        sendPacketToGlobal(user, data);
        print('Sending packet to user '+user.id.toString()+', aka '+user.lanUser.lan_id);
      }
    });
  }

  static decodeConversation(List<int> data) {

    Conversation conversation;

    if (data[0] == DataType.conversationId.index) {
      int id = Convert.decodeInt32(data.sublist(1,1+Convert.Int32Size));
      conversation = Conversation.findConversationById(Data.conversations, id);

      if (data[1+Convert.Int32Size] == DataType.conversationType.index) {
        if (data[3+Convert.Int32Size] == DataType.globalUser.index) {
          if (data[4+Convert.Int32Size] == DataType.length.index) {

            int size = Convert.decodeInt32(data.sublist(5+Convert.Int32Size, 5+2*Convert.Int32Size));
            GlobalUser creator = Convert.decodeGlobalUser(data.sublist(3+Convert.Int32Size, 3+Convert.Int32Size+size));

            print(3+Convert.Int32Size+size);
            if (data[3+Convert.Int32Size+size] == DataType.globalUserList.index) {
              List<GlobalUser> users = Convert.decodeGlobalUserList(data.sublist(4+Convert.Int32Size+size));

              if (data[2+Convert.Int32Size] == ConversationType.private.index) {
                String title = creator.name;

                if (conversation != null) {
                  //TODO : check things and (maybe) add missing members
                  print('ERROR : This is a reminder to implement things that should be there');
                }
                else {
                  conversation = new Conversation(id: id, isNew: true, title: title, isOffline: true, user : creator, isPrivate: true);
                }
                creator.privateConversation = conversation;
              }
              else {
                throw new Exception('Conversation type not implemented');
              }
            }
            else {
              throw new Exception('Trying to decode malformed conversation');
            }
          }
          else {
            throw new Exception('Trying to decode malformed conversation');
          }
        }
        else {
          throw new Exception('Trying to decode malformed conversation');
        }
      }
      else {
        throw new Exception('Trying to decode malformed conversation');
      }
    }
    else {
      throw new Exception('Trying to decode malformed conversation');
    }
    return conversation;
  }

  static Message decodeComplexMessage(List<int> data) {

    Message message;
    Conversation conversation;

    if (data[0] == DataType.conversationId.index && data[1+Convert.Int32Size] == DataType.id.index && data[2+2*Convert.Int32Size] == DataType.message.index) {
      int conversationId = Convert.decodeInt32(data.sublist(1, 1+Convert.Int32Size));
      int userId = Convert.decodeInt32(data.sublist(2+Convert.Int32Size,2+2*Convert.Int32Size));
      String messageText = Convert.decodeString(data.sublist(3+2*Convert.Int32Size));

      conversation = Conversation.findConversationById(Data.conversations, conversationId);
      if (conversation != null) {
        GlobalUser user = GlobalUser.findUserById(conversation.users, userId);
        if (user != null) {
          message = Message(user : user, message: messageText, date: DateTime.now().toIso8601String(), name: user.name, conversation: conversation,);
          conversation.add(message); //TODO : implement storing in the database
        }
        else {
          throw new Exception('Received messsage in a conversation from a user that does not belong to this conversation');
        }
      }
      else {
        throw new Exception('Received a complex message that targets unknown conversation');
      }
    }
    else {
      throw new Exception('Trying to decode malformed complex message');
    }
    return message;
  }
  
  static void sendMessageFileTo(String lanId, MessageFile file) async {
     file.payloadId = Convert.abs(await Nearby().sendFilePayload(lanId, (await file.file).path));
     
     List<int> data = [];
     data.add(PacketType.fileInfo.index);
     data.add(DataType.id.index);
     data.addAll(Convert.encodeInt64(file.payloadId));
     data.add(DataType.conversation.index);
     data.addAll(Convert.encodeInt32(file.message.conversation.id));
     data.add(DataType.message.index);
     data.addAll(Convert.encodeInt32(file.message.id));
     data.add(DataType.name.index);
     data.addAll(Convert.encodeString(file.filename));
     
     Nearby().sendBytesPayload(lanId, Uint8List.fromList(data));
  }
  
  static void processFileInfo(List<int> data) async{
    if (data[0] == DataType.id.index && data[1+Convert.Int64Size] == DataType.conversation.index
        && data[2+Convert.Int32Size * 1 + Convert.Int64Size] == DataType.message.index
        && data[3+Convert.Int32Size * 2 + Convert.Int64Size] == DataType.name.index) {
      MessageFile file = MessageFile.findFileByPayloadId(Data.files,  Convert.decodeInt64(data.sublist(1,1+Convert.Int64Size)));
      if (file == null) {
        print('Delayed file info processing');
        print('This payloadid : '+ Convert.decodeInt64(data.sublist(1,1+Convert.Int64Size)).toString() );
        Data.files.forEach((file) {
          print('File : '+file.payloadId.toString());
        });
        Timer(Duration(seconds: 1), () {
          processFileInfo(data);
        });
      }
      else {
        Conversation conversation = Conversation.findConversationById(Data.conversations, Convert.decodeInt32(data.sublist(2+Convert.Int64Size, 2+ Convert.Int32Size*1 + Convert.Int64Size)));
        if (conversation != null) {
          Message message = Message.findMessageById(conversation.messages, Convert.decodeInt32(data.sublist(3+Convert.Int32Size*1 + Convert.Int64Size, 3+ Convert.Int32Size*2 + Convert.Int64Size)));
          if (message != null) {
           file.file.then((File value) {
             message.file = value;
           });
          }
          else {
            print('ERROR : received file for unknown message '+Convert.decodeInt32(data.sublist(3+Convert.Int32Size*1 + Convert.Int64Size, 3+ Convert.Int32Size*2 + Convert.Int64Size)).toString());
            Timer(Duration(seconds: 1), () {
              processFileInfo(data);
            });
          }
        }
        else {
          print ('ERROR : received file info for unknown conversation');
        }
      }
    }
    else {
      print('ERROR : received malformed fileinfo payload');
    }
  }
}


