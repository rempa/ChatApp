import 'package:flutter/material.dart';
import 'Data.dart';
import 'User.dart';

class UserManager {
  static final userList = Data.user.connectedTo;
  static final GlobalKey<AnimatedListState> listKey = GlobalKey();

  static add(User user) {
    userList.add(user);

    listKey.currentState.insertItem(userList.length - 1);
    listKey.currentState.insertItem(userList.length);
  }

  static remove(User user) {
    userList.remove(user);

    //TODO : implement display
  }

}