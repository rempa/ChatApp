
import 'package:flutter/material.dart';
import 'package:nearby_connections/nearby_connections.dart';
import 'MessageBox.dart';
import 'MessagesList.dart';
import 'UserList.dart';
import 'ConnectionController.dart';
import 'ChangeName.dart';
import 'Data.dart';
import 'ConversationList.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();

  while (!await Nearby().checkLocationPermission())
    await Nearby().askLocationPermission();

  while(!await Nearby().checkExternalStoragePermission())
    await Nearby().askExternalStoragePermission();

  await Data.init();

  ConnectionController.start();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: MyHome()
    );
  }
}

class MyHome extends StatefulWidget {

  @override
  MyHomeState createState() => MyHomeState();
}

class MyHomeState extends State<MyHome> {

  MyHomeState() {
    Data.homeState = this;
  }

  List<Widget> _messages = Data.currentConversation.messages;

  final ThemeData mainTheme = ThemeData(
    brightness: Brightness.dark,
    primaryColor: Colors.black,
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Local Chat',
      theme: mainTheme,
      home: Scaffold(
        appBar: AppBar(
          title: Text(Data.currentConversation.title),
          actions: <Widget>[
            IconButton(icon: Icon(Icons.edit), onPressed: (){ _pushSaved(context); }),
          ],

        ),
        body: Column(
          children: [
            Expanded(
                child: MessagesList(messagesList: _messages,)
            ),
            MessageBox(),
          ],
        ) ,
        endDrawer: new Drawer(
            child: UserList()
        ),
        drawer: new Drawer(
          child: ConversationList(),
        ),
      ),
    );
  }

  void updateMessages() {
    setState(() {
      _messages = Data.currentConversation.messages;
    });
  }

  void _pushSaved(context) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          return Scaffold(
            appBar: AppBar(
              title: Text('Changer de nom'),
              backgroundColor: mainTheme.primaryColor,
              brightness: mainTheme.brightness,
            ),
            backgroundColor: mainTheme.scaffoldBackgroundColor,
            body: Column(
              children: [
                ChangeName(),
              ],
            ),
          );
        },
      ),
    );
  }
}
