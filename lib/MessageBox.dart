
import 'package:flutter/material.dart';
import 'dart:async';
import 'Data.dart';
import 'GlobalUser.dart';
import 'SendFile.dart';

class MessageBox extends StatefulWidget {
  @override
  MessageBoxState createState() => MessageBoxState();

}

class MessageBoxState extends State<MessageBox> {
  final _formKey = GlobalKey<FormState>();

  final messageTextController = TextEditingController();

  @override
  void dispose() {
    messageTextController.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          TextFormField(
            validator: (value) {
              if (value.isEmpty) {
                return 'Merci d\'entrer du texte';
              }
              if (Data.currentConversation.users.length == 2) {
                if ((Data.currentConversation.users[0] == Data.globalUser || (Data.currentConversation.users[0].lanUser != null && Data.currentConversation.users[0].lanUser.connected == true)) && (Data.currentConversation.users[1] == Data.globalUser || (Data.currentConversation.users[1].lanUser != null && Data.currentConversation.users[1].lanUser.connected == true))) {

                }
                else {
                  return 'Vous n''êtes pas connecté à cet utilisateur';
                }
              }
              return null;
            },
            controller: messageTextController,
            decoration: InputDecoration(
              hintText: 'Message',
              suffixIcon: IconButton(
                  icon: Icon(Icons.send),
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      Data.currentConversation.send(messageTextController.text);
                      Timer(new Duration(milliseconds: 30), () {
                        messageTextController.text = "";
                      });

                    }
                  }
              ),
                prefixIcon: IconButton(
                    onPressed: () async{
                        Data.currentConversation.send(messageTextController.text, file : await SendFile.getImage());
                        Timer(new Duration(milliseconds: 30), () {
                          messageTextController.text = "";
                        });

                    }
                  ,
                  tooltip: "Choisir une image",
                  icon: Icon(Icons.add_a_photo),
                )

            ),
            style: TextStyle(fontSize: 22),
            maxLines: null,
          ),
        ],
      ),
    );
  }
}