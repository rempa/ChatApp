import 'User.dart';
import 'Conversation.dart';
import 'Data.dart';
import 'package:sqflite/sqflite.dart';
import 'package:flutter/foundation.dart';

class GlobalUser {

  int _id;
  String _name;
  bool legit = false;
  Conversation privateConversation;

  String pub_key;

  User _lanUser;

  int get id {
    return _id;
  }

  String get name {
    return _name;
  }

  set name(String value) {
    if (this == Data.globalUser) {
      _name = value;
      update();
    }
    else {
      throw new Exception('You are not allowed to modify this user''s name');
    }
  }

  User get lanUser{
    return _lanUser;
  }

  set lanUser(User value) {
    _lanUser = value;
  }

  setName(String name, {String proof}) {
    _name = name;
    update();
  }

  GlobalUser({@required int id, @required String pub_key, @required String name, bool isNotNew}) {
    _id = id;
    this.pub_key = pub_key;
    this.setName(name);

    if (isNotNew != null && isNotNew) {

    }
    else {
      insert();
    }
  }

  Map<String, dynamic> toMap() {
    if (privateConversation != null) {
      return {
        'id': id,
        'name': name,
        'pub_key' : pub_key,
        'private_conversationid' : privateConversation.id
      };
    }
    else {
      return {
        'id': id,
        'name': name,
        'pub_key' : pub_key,
        'private_conversationid' : null
      };
    }
  }

  Future<void> insert() async {
    await Data.database.insert(
      'user',
      this.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> update() async {
    await Data.database.update(
      'user',
      this.toMap(),
      where: "id = ?",
      whereArgs: [id],
    );
  }

  checkSignature(String signature) { //temporarily empty
    return true;
  }

  static GlobalUser findUserById(List<GlobalUser> users, int id) {
    GlobalUser theUser;
    for (GlobalUser user in users) {
      if (user != null && user.id == id) {
        theUser = user;
        break;
      }
    }
    return theUser;
  }
}