
import 'Conversation.dart';
import 'Data.dart';
import 'GlobalUser.dart';
import 'MessagesList.dart';
import 'dart:async';
import 'package:flutter/material.dart';

class ConversationController {

  static switchToPrivateConversation({GlobalUser user}) {

    if (user != null) {
      if (user.privateConversation == null) {
        Conversation conversation = new Conversation(title: user.name,
            isOffline: true,
            user: user); //TODO : change this when online will be implemented
        user.privateConversation = conversation;
        Data.conversations.add(conversation);
      }

      Data.currentConversation = user.privateConversation;
      Data.homeState.updateMessages();

      MessagesListState scrollController = (Data.listKey.currentContext.ancestorStateOfType(new TypeMatcher()) as MessagesListState);
      Timer(new Duration(milliseconds: 50), () {
        scrollController.switchConversation();

      });

      // The code to refresh the listView should be here


    }
    else {
      throw new Exception('ERROR : Tried to switch to private conversation without giving a user as a parameter');
    }

  }

  static switchToConversation(Conversation conversation) {
    Data.currentConversation = conversation;
    Data.homeState.updateMessages();

    final scrollController = (Data.listKey.currentContext.ancestorStateOfType(new TypeMatcher()) as MessagesListState);
    Timer(new Duration(milliseconds: 50), () { //without this delay, the last message isn't loaded in the listview properly and it doesn't scrolls properly to the last message

      scrollController.switchConversation();
    });
  }
}