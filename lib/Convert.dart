
import 'Enum.dart';
import 'GlobalUser.dart';
import 'Data.dart';

class Convert {
  static int abs(int value) {
    if (value < 0)
      return -value;
    else
      return value;
  }

  static List<int> encodeInt16(int value) {
    return <int>[ value ~/ 256, value % 256 ];
  }

  static List<int> encodeInt16List(List<int> list) {
    List<int> result = [];
    for (int i in list) {
      result.addAll(encodeInt16(i));
    }
    return result;
  }

  static int decodeInt16(List<int> list) {
    return list[0] * 256 + list[1];
  }

  static List<int> decodeInt16List(List<int> list) {
    List<int> result = [];
    for (int i = 0 ; i < list.length ~/ 2 ; i++) {
      result.add(decodeInt16(list.sublist(i*2, i*2+2)));
    }
    return result;
  }
  
  static List<int> encodeInt32(int value) {
    return encodeInt16List(<int>[ value ~/ 65536, value %65536]);
  }

  static List<int> encodeInt32List(List<int> list) {
    List<int> result = [];
    for (int i in list) {
      result.addAll(encodeInt32(i));
    }
    return result;
  }
  
  static int decodeInt32(List<int> list) {
    List<int> temp = decodeInt16List(list);
    return temp[0] * 65536 + temp[1];
  }

  static List<int> decodeInt32List(List<int> list) {
    List<int> result = [];
    for (int i = 0 ; i < list.length ~/ Convert.Int32Size ; i++) {
      result.add(decodeInt32(list.sublist(i*Convert.Int32Size, i*Convert.Int32Size+Convert.Int32Size)));
    }
    return result;
  }
  
  static List<int> encodeInt64(int value) {
    return encodeInt32List(<int>[ value ~/ 4294967296, value % 4294967296]);
  }

  static int decodeInt64(List<int> list) {
    List<int> temp = decodeInt32List(list);
    return temp[0] * 4294967296 + temp[1];
  }

  static List<int> encodeString(String message) {
    List<int> data = [];
    data.addAll(encodeInt16List(message.codeUnits));
    return data;
  }

  static String decodeString(List<int> data) {
    return new String.fromCharCodes(decodeInt16List(data));
  }
  
  static List<int> encodeGlobalUser(GlobalUser user) {
    List<int> data = [];
    data.add(DataType.globalUser.index);
    data.add(DataType.length.index);
    data.addAll(encodeInt32(0));
    data.add(DataType.id.index);
    data.addAll(encodeInt32(user.id));
    data.add(DataType.name.index);
    data.addAll(encodeString(user.name));
    
    int size = data.length;
    List<int> encodedSize = encodeInt32(size);
    /*data.removeRange(2, 1+ Convert.Int32Size);
    data.insertAll(2, encodeInt32(size));*/
    //TODO : make this less ugly
    data[2] = encodedSize[0];
    data[3] = encodedSize[1];
    data[4] = encodedSize[2];
    data[5] = encodedSize[3];

    return data;
  }
  
  static GlobalUser decodeGlobalUser(List<int> data) {

    GlobalUser user;

    if (data[0] == DataType.globalUser.index && data[1] == DataType.length.index) {
      if (data.length == Convert.decodeInt32(data.sublist(2, 2+Convert.Int32Size))) {
        if (data[2+Convert.Int32Size] == DataType.id.index) {
          if (data[3+2*Convert.Int32Size] == DataType.name.index) {
            int id = Convert.decodeInt32(data.sublist(3+Convert.Int32Size, 3+2*Convert.Int32Size));
            String name = Convert.decodeString(data.sublist(4+2*Convert.Int32Size));

            user = GlobalUser.findUserById(Data.users, id);
            if (user == null) {
              user = new GlobalUser(id: id, pub_key: '', name: name);
              Data.users.add(user);
            }
          }
          else {
            throw new Exception('Tried, to decode malformed globalUser');
          }
        }
        else {
          throw new Exception('Tried, to decode malformed globalUser');
        }
      }
      else {
        throw new Exception('Tried to decode a user that has a length that does not match given data length');
      }
    }
    else {
      throw new Exception('Tried, to decode malformed globalUser');
    }
    return user;
  }

  static List<int> encodeGlobalUserList(List<GlobalUser> users) {
    List<int> data = [DataType.globalUserList.index, DataType.length.index];
    users.forEach((user) {
      data.addAll(encodeGlobalUser(user));
    });
    data.insertAll(2, Convert.encodeInt32(data.length+Convert.Int32Size));

    return data;
  }

  static List<GlobalUser> decodeGlobalUserList(List<int> data) {
    List<GlobalUser> list = [];

    if (data[0] == DataType.globalUserList.index && data[1] == DataType.length.index) {
      if (data.length == Convert.decodeInt32(data.sublist(2, 2+Convert.Int32Size))) {
        List<int> sublist = data.sublist(2+Convert.Int32Size);

        while (sublist.length > 0) {
          if (sublist[0] == DataType.globalUser.index && sublist[1] == DataType.length.index) {
            int length = decodeInt32(sublist.sublist(2, 2+Convert.Int32Size));
            List<int> user = sublist.sublist(0, length);
            sublist = sublist.sublist(length);

            list.add(decodeGlobalUser(user));
          }
          else {
            throw new Exception('Tried to decode malformed globalUserList');
          }
        }
      }
      else {
        throw new Exception('Tried to decode malformed globalUserList');
      }
    }
    else {
      throw new Exception('Tried to decode malformed globalUserList');
    }
    return list;
  }
  
  static const int Int8Size = 1;
  static const int Int16Size = Int8Size * 2;
  static const int Int32Size = Int16Size * 2;
  static const int Int64Size = Int32Size * 2;
}