import 'ConnectionController.dart';
import 'Data.dart';
import 'Message.dart';
import 'GlobalUser.dart';


class User {

  String _name = 'Bibi';
  String lan_id;
  bool _directlyConnected = false;
  bool _connected = false;
  String route;
  List<User> connectedTo = [];
  GlobalUser _globalUser;


  User({String name}) {
    this._name = name;
  }

  String get name {
    if (_name != null)
      return _name;
    else
      return 'Unknown username';
  }

  set name(String value) {
    if (this == Data.user) {
      Data.database.update('this_user', {'name' : value});
      Data.lanConversation.add(SystemMessage('Vous vous êtes renommé en '+value));
      ConnectionController.sendName(value);
    }
    else if (value != null) {
      if (globalUser != null) {
        globalUser.setName(value);
      }
      if (_name == null) {
        Data.lanConversation.add(
            SystemMessage(value + ' a rejoint le chat !'));
      }
      else {
        Data.lanConversation.add(
          SystemMessage(_name + " s'est renommé en " + value));
      }
    }
    _name = value;
  }

  bool get directlyConnected {
    return _directlyConnected;
  }

  set directlyConnected(bool value) {
    _directlyConnected = value;
    if (value) {
      connected = true;
    }
  }

  bool get connected {
    return _connected;
  }

  set connected(bool value) {
    if (value != _connected) {
      if (value && !_connected) {
        ConnectionController.sendNewUserConnectedNotice(this);
      }
      else if (_connected) {
        Data.lanConversation.add(SystemMessage(this.name+' est parti'));
      }
    }
    _connected = value;
  }

  GlobalUser get globalUser {
    return _globalUser;
  }

  set globalUser (GlobalUser value) {
    _globalUser = value;
  }

  static User findUserByLanId(List<User> users, String lan_id) {
    User theUser;
    for (User user in users) {
      if (user.lan_id.toString() == lan_id.toString()) {
        theUser = user;
        break;
      }
    }
    return theUser;
  }

  bool addConnectedUser(User user) {
    bool found = false;
    this.connectedTo.forEach((connectedUser) {
      if (user.lan_id == connectedUser.lan_id) {
        if (user.directlyConnected && !connectedUser.directlyConnected) {
          print('Looks like we are directly connected to '+user.lan_id);
          connectedUser.directlyConnected = true;
        }
        connectedUser.connected = true;
        found = true;
      }
    });
    if (!found && user.lan_id != Data.user.lan_id) {
      print('This is indeed a new user, indirectly connected '+user.lan_id);
      user.connected = true;
      this.connectedTo.add(user);
      if (user.directlyConnected)
        Data.lanUsers.add(user);
    }
    return !found;
  }

}