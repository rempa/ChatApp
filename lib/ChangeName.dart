import 'package:flutter/material.dart';
import 'ConnectionController.dart';
import 'Data.dart';


class ChangeName extends StatefulWidget {
  ChangeNameState createState() => ChangeNameState();
}

class ChangeNameState extends State<ChangeName> {


  final _formKey = GlobalKey<FormState>();
  final nameTextController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    nameTextController.dispose();
    super.dispose();
  }


  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
          children: <Widget>[
            TextFormField(
              validator: (value) {
                if (value.isEmpty) {
                  return 'Merci d\'entrer un nom';
                }
                return null;
              },
              controller: nameTextController,
              decoration: InputDecoration(
                hintText: 'Pseudal',
                hintStyle: TextStyle(fontSize: 22, color: Colors.white30),
                suffixIcon: IconButton(
                    icon: Icon(Icons.send, color: Colors.white,),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        Data.user.name = nameTextController.text;
                        Navigator.pop(context);
                      }
                    }
                ),
              ),
              style: TextStyle(fontSize: 22, color: Colors.white),
              maxLines: null,
            ),
          ],
      ),
    );
  }
}
