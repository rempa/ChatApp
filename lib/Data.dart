
import 'User.dart';
import 'GlobalUser.dart';
import 'Enum.dart';
import 'GlobalUser.dart';
import 'Conversation.dart';
import 'MessagesList.dart';
import 'MessageFile.dart';
import 'main.dart';
import 'dart:math';
import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:flutter/material.dart';


class Data {

  static init() async {
    random = Random();

    database = await openDatabase(
      join(await getDatabasesPath(), 'database.db'),
        onCreate: (db, version) {
          db.execute(Constants.DBCreateTableThisUser);
          db.execute(Constants.DBCreateTableUser);
          db.execute(Constants.DBCreateTableConversation);
          db.execute(Constants.DBCreateTableConversationUser);
          db.execute(Constants.DBCreateTableMessage);

          int id = random.nextInt(4294967295); //TODO : use a constant
          db.insert('this_user', {'id':id, 'name':'Bibi'});
          db.insert('user', {'id' : id, 'name':'Bibi', 'pub_key':''}); //TODO : implement keys generation
        },
      version: 1,
    );

    List<Map<String, dynamic>> maps = await database.query('this_user');
    int id = maps[0]['id'];

    user = new User(
        name: maps[0]['name'],
    );

    maps = await database.query('user');

    users = List.generate(maps.length, (i) {
      print(maps[i]['name']);
      return GlobalUser(
        id: maps[i]['id'],
        name: maps[i]['name'],
        pub_key: maps[i]['pub_key'],
        isNotNew: true
      );
    });

    globalUser = GlobalUser.findUserById(users, id);
    globalUser.name = user.name; //TODO : temporary, should be fixed when this_user will be inserted properly in the database

    lanConversation = new Conversation(id: 0, title: 'Local Chat');
    currentConversation = lanConversation;

    conversations.add(lanConversation);

    maps = await database.query('conversation');
    conversations.addAll(List.generate(maps.length, (i) {
      if (maps[i]['id'] != 0 && maps[i]['id'] != null) {
        return Conversation(id : maps[i]['id'], title : maps[i]['title']);
      }
      else {
        return Conversation(id : maps[i]['temp_id'], isOffline: true, title : maps[i]['title']);
      }
    }));


    lanUsers = [];

    isDiscovering = false;
    isAdvertising = false;
  }

  static Database database;
  static User user;
  static GlobalUser globalUser;
  static Conversation lanConversation;
  static Conversation currentConversation;

  static List<User> lanUsers;
  static List<GlobalUser> users;
  static List<Conversation> conversations = [];
  static List<MessageFile> files = [];

  static Random random;
  static Timer timer;

  static bool isDiscovering;
  static bool isAdvertising;

  static MyHomeState homeState;
  static final GlobalKey<AnimatedListState> listKey = GlobalKey();

}