
import 'package:flutter/material.dart';
import 'Data.dart';

class MessagesList extends StatefulWidget {
  @override
  MessagesListState createState() => MessagesListState();

  final List<Widget> messagesList;
  MessagesList({@required List<Widget> this.messagesList});
}

class MessagesListState extends State<MessagesList> {

  ScrollController _scrollController = ScrollController();

  void initState() {
    super.initState();
    _scrollController = ScrollController();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  scrollToBottom() {
    _scrollController.jumpTo(_scrollController.position.maxScrollExtent,
       /* duration: Duration(milliseconds: 200), curve: Curves.easeOut*/);
  }

  switchConversation() {
    _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
  }


  Widget build(BuildContext context) {
    return AnimatedList(
      //reverse: true,
        key: Data.listKey,
        controller: _scrollController,
        initialItemCount: widget.messagesList.length * 2,
        itemBuilder: (context, i, animation) {
          if (widget.messagesList.length *2>i) {
            if (i.isOdd)
              return Divider(height: 1, thickness: 1, color: Colors.white70,);

            final index = i ~/ 2;

            return widget.messagesList[index];
          }
          else {
            return null;
          }
        }
    );
  }
}