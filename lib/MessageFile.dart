import 'dart:io';
import 'Message.dart';
import 'Convert.dart';

class MessageFile {
  int id;
  int _payloadId;
  File _file;
  Message message;
  String filename;
  bool isTransferring = false;

  Future<File> get file async {
   /* print('yo');
    throw new Exception('wowowo');*/
    while (isTransferring && _file != null) {
      sleep(Duration(seconds: 1));
      print('wtf ?');
    }
    return this._file;
  }

  int get payloadId {
    return _payloadId;
  }

  set payloadId(int value) {
    _payloadId = Convert.abs(value);
    //TODO : make this cleaner by properly encoding signed integers
  }

  MessageFile(File file,{this.message, bool isTransferring}) {
    if (isTransferring == null)
      this.isTransferring = false;
    else
      this.isTransferring = isTransferring;

    this._file = file;
    filename = file.path.split("/").last;
  }

  static MessageFile findFileByPayloadId(List<MessageFile> files, int id) {

    for (MessageFile file in files) {
      if (file.payloadId == id) {
        return file;
      }
    }
  }

}