import 'package:image_picker/image_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'Data.dart';
import 'Message.dart';

class SendFile {

  static Future getImage() async {
    return await ImagePicker.pickImage(source: ImageSource.gallery) ;
  }


}