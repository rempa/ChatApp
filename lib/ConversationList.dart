import 'package:flutter/material.dart';
import 'UserManager.dart';
import 'Data.dart';
import 'ConversationController.dart';


class ConversationList extends StatefulWidget {
  @override
  ConversationListState createState() => ConversationListState();
}

class ConversationListState extends State<ConversationList> {
  @override
  Widget build(BuildContext context) {
    return AnimatedList(
        initialItemCount: (Data.conversations.length +1) *2,
        itemBuilder: (context, i, animation) {

          if (i.isOdd) return Divider(height: 1, thickness: 1, color: Colors.white70, );

          final index = i ~/ 2;
          if (index == 0) {
            return ListTile(
                title : Text('Conversations : ', textScaleFactor: 1.5,)
            );
          }
          else {
            return ListTile(
              title : Text(Data.conversations[index -1].title),
              onTap: () {
                ConversationController.switchToConversation(Data.conversations[index -1]);
                Navigator.of(context).pop();
              },
              //subtitle: Text(UserManager.userList[index-2].lan_id.toString()+', '+UserManager.userList[index-2].route.toString()+', '+UserManager.userList[index-2].directlyConnected.toString()+ ', '+UserManager.userList[index-2].globalUser.id.toString()),
            );
          }
        }
    );
  }
}