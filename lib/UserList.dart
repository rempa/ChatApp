import 'package:flutter/material.dart';
import 'UserManager.dart';
import 'Data.dart';
import 'ConversationController.dart';
import 'Convert.dart';

class UserList extends StatefulWidget {
  @override
  UserListState createState() => UserListState();
}

class UserListState extends State<UserList> {
  @override
  Widget build(BuildContext context) {
    if (Data.currentConversation.id == 0) {
      return AnimatedList(
          key: UserManager.listKey,
          initialItemCount: Data.user.connectedTo.length * 2 + 4,
          itemBuilder: (context, i, animation) {

            if (i.isOdd) return Divider(height: 1, thickness: 1, color: Colors.white70, );

            final index = i ~/ 2;
            if (index == 0) {
              return ListTile(
                  title : Text('Liste des utilisateurs connectés :', textScaleFactor: 1.25,)
              );
            }
            else if (index == 1) {
              return ListTile(
                  title : Text(Data.user.name+ ' (Vous)'),
              );
            }
            else {
              return ListTile(
                title : Text(Data.user.connectedTo[index-2].name),
                onLongPress: () {
                  ConversationController.switchToPrivateConversation(user : Data.user.connectedTo[index - 2].globalUser); //TODO : check that it is not null
                },
                //subtitle: Text(UserManager.userList[index-2].lan_id.toString()+', '+UserManager.userList[index-2].route.toString()+', '+UserManager.userList[index-2].directlyConnected.toString()+ ', '+UserManager.userList[index-2].globalUser.id.toString()),
              );
            }
          }
      );
    }
    else {
      return AnimatedList(
          key: UserManager.listKey,
          initialItemCount: Data.currentConversation.users.length * 2 + 2,
          itemBuilder: (context, i, animation) {

            final index = i ~/ 2;

            if (Data.currentConversation.users.length > index-1) {
              if (i.isOdd) return Divider(height: 1, thickness: 1, color: Colors.white70, );
              if (index == 0) {
                return ListTile(
                    title : Text('Liste des utilisateurs :', textScaleFactor: 1.25,)
                );
              }
              else {
                return ListTile(
                  title : Text(Data.currentConversation.users[index-1].name),
                  onLongPress: () {
                    ConversationController.switchToPrivateConversation(user : Data.currentConversation.users[index - 1]); //TODO : check that it is not null
                  },
                  //subtitle: Text(UserManager.userList[index-2].lan_id.toString()+', '+UserManager.userList[index-2].route.toString()+', '+UserManager.userList[index-2].directlyConnected.toString()+ ', '+UserManager.userList[index-2].globalUser.id.toString()),
                );
              }
            }
            else {
              return null;
            }

          }
      );
    }

  }
}