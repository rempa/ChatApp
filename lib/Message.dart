
import 'package:flutter/material.dart';
import 'GlobalUser.dart';
import 'Conversation.dart';
import 'Data.dart';
import 'dart:io';

class SystemMessage extends StatefulWidget {

  String _message;

  SystemMessage(message) {
    _message = message;
  }

  SystemMessageState createState() => SystemMessageState(_message);
}

class SystemMessageState extends State<SystemMessage> {
  String _message;

  SystemMessageState(message) {
    _message = message;
  }

  Widget build(BuildContext context) {
    return ListTile(
      title: Text(_message, style: TextStyle(color: Colors.white30, fontStyle: FontStyle.italic),),
    );
  }
}

class Message extends StatefulWidget {

  @override
  MessageState createState() {
    currentState = MessageState(_file);
    return currentState;
  }

  MessageState currentState;

  int id;
  final String name;
  File _file;
  final String message;
  final String date;
  final GlobalUser user;
  final Conversation conversation;
  bool sent;

  File get file {
    return _file;
  }

  set file (File file) {
    this._file = file;
    if (currentState != null) {
      currentState.updateFile();
    }
  }

  Message({this.id, @required this.name, @required this.message, @required this.date, this.sent, this.user, @required this.conversation, File file}) {
    this._file = file;
    if (id == null) {
      this.id = conversation.nextId();
    }
  }
  
  insert() async {
    await Data.database.insert('message', toMap());
  }

  Map<String, dynamic> toMap() {

    return {
      'id' : id,
      'idconversation': conversation.id,
      'iduser': user.id,
      'date': date,
      'message' : message
    };
  }

  static findMessageById(List<Widget> messages, int id) {
    for (Widget message in messages) {
      if (message is Message) {
        if (message.id == id) {
          return message;
        }
      }
    }
  }
}

class MessageState extends State<Message> {

  File file;

  MessageState(File file) {
    this.file = file;
  }

  void updateFile() {
    setState(() {
      this.file = widget.file;
    });
  }

  Widget build(BuildContext context) {
    if (file == null) {
      return ListTile(
        title: Text(widget.name),
        subtitle: Text(widget.message),

      );
    }
    else {
      return Column(
          children: [
            ListTile(
                title: Text(widget.name),
                subtitle: Text(widget.message)
            ),
            Image.file(file)
          ]
      );
    }
  }
}
